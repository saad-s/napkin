# Napkin

### Instructions

- Install the dependencies with `yarn install`. This is only required if you want to run the live reloading development server.
- Start the development server with `yarn start`.
- To create a build, run `yarn build` (or `npm run build`). This simply copies `img`, `css`, `js` directories and `index.html` to a new directory `dist`.

**`npm` can be used instead of `yarn` if that's what you prefer.**
